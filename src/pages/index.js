import React from "react"
import Layout from "../components/layout"
import { graphql } from "gatsby"

const IndexPage = ({ data }) => (
  <Layout>
    <h1>Bands</h1>
    <table>
      <tr><td>ID</td><td>TITLE</td><td>WEBSITE</td></tr>      
      {data.allContentfulBand.edges.map(({ node, index }) => (
       <tr> 
        <td>
          {node.id}
        </td>
        <td>
          {node.name}
        </td>
        <td>
          {node.website}
        </td>
        </tr> 
      ))}      
    </table>    
  </Layout>
)

export default IndexPage

export const query = graphql`
  {
    allContentfulBand {
      edges {
        node {
          id
          name
          website
        }
      }
    }
  }
`